package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение доступных команд.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand cmd: commands)
            System.out.println(cmd);
    }

}
